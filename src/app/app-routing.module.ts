import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import { HomePageComponent } from './pages/home/home.component'
import { AboutUsPageComponent } from './pages/about-us/AboutUs.component'
import { ProductPageComponent } from './pages/product/product.component'
import { CatalogPageComponent } from './pages/catalog/catalog.component'
import { ContactPageComponent } from './pages/contact/contact.component'
import { CartPageComponent } from './pages/cart/cart.component'
import { CategoryPageComponent } from './pages/category/category.component'

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'about-us', component: AboutUsPageComponent },
  { path: 'contact', component: ContactPageComponent },
  { path: 'category', component: CategoryPageComponent },
  { path: "catalog/:category/:page", component: CatalogPageComponent },
  { path: "product/:id", component: ProductPageComponent },
  { path: 'cart', component: CartPageComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}