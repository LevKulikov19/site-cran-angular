import { Component, Input, OnInit } from '@angular/core';

interface IBenifit {
    title: string,
    description: string,
    img: string
}

@Component({
    selector: 'app-home-benefits',
    templateUrl: './home-benefits.component.html',
    styleUrls: ['./home-benefits.component.scss']
})


export class HomeBenefitsComponent implements OnInit {
    @Input() items: Array<any>;
    item: Array<object>
    constructor() { }
    ngOnInit(): void {
        this.item = [
            {
                img: "/file/icon-a-1.png",
                discription: "Более чем 20-летний опыт",
                title: "Опыт работы"
            }
        ]
    }
}
