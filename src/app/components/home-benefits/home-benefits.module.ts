import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'

import { HomeBenefitsComponent } from './home-benefits.component'
import { HomeBenefitsItemComponent } from '../home-benefits-item/home-benefits-item.component'



@NgModule({
  declarations: [
    HomeBenefitsItemComponent
  ],
  imports: [],
  providers: [],
  bootstrap: [HomeBenefitsComponent]
})
export class AppModule {
}