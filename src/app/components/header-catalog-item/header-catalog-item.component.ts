import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-header-catalog-item',
  templateUrl: './header-catalog-item.component.html',
  styleUrls: ['./header-catalog-item.component.scss']
})


export class HeaderCatalogItemComponent {
  @Input() name: string;
  @Input() imgSrc: string;
  @Input() imgAlt: string;
  @Input() link: string;
}
