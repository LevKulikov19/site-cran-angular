import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import formatPrice from '../../utils/formatPrice';
import { CartService } from '../../context/CartContext';


@Component({
  selector: 'app-product-modal-card',
  templateUrl: './product-card-modal.component.html',
  styleUrls: ['./product-card-modal.component.scss']
})
export class ProductCardMoadalComponent implements OnInit {
  @Input() product: any;
  @Output() close = new EventEmitter<boolean>();
  ID: number;
  Name: string;
  URLImage: string;
  Category: string;
  TTX1: string;
  TTX2: string;
  Brand: string;
  Count: number;
  Price: number;

  Url: string;
  PriceVisible: number;
  quantity: number = 1;

  cartService: CartService;

  constructor() { }


  ngOnInit(): void {
    this.cartService = new CartService();

    console.log(this.product);
    this.ID = this.product.ID;
    this.Name = this.product.Name;
    this.URLImage = this.product.URLImage;
    this.Category = this.product.Category;
    this.TTX1 = this.product.TTX1;
    this.TTX2 = this.product.TTX2;
    this.Brand = this.product.Brand;
    this.Count = this.product.Count;
    this.Price = this.product.Price;

    this.Url = '/product/' + this.ID.toString();
    this.PriceVisible = formatPrice(this.Price);
  }

  addToCart() {
    this.cartService.addItem(this.product, this.quantity);
  }

  closeModal() {
    this.close.emit(true);
  }

  onInputQuantity(event) {
    this.quantity = event.target.value;
  }
}
