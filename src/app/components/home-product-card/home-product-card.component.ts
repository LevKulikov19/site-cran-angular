import { Component, Input, OnInit } from '@angular/core';

interface IBenifit {
    title: string,
    description: string,
    img: string
}

@Component({
    selector: 'app-home-product-card',
    templateUrl: './home-product-card.component.html',
    styleUrls: ['./home-product-card.component.scss']
})


export class HomeProductCardComponent implements OnInit {
    item: Array<object>

    product1: object
    product2: object
    product3: object
    product4: object

    constructor() { }
    ngOnInit(): void {
        this.product1 = {
            "ID": 1,
            "Name": "Экскаватор погрузчик MST M542",
            "URLImage": "assets/file/category-special-equipment-1.jpg",
            "Category": "Экскаваторы погрузчики",
            "TTX1": "Мощность двигателя: 101 л.с.",
            "TTX2": "Экскаваторный ковш: 0.26 м",
            "Brand": "MST (Турция)",
            "Count": 1000,
            "Price": 8211650
        };
        this.product2 =
        {
            "ID": 2,
            "Name": "Экскаватор погрузчик MST M542",
            "URLImage": "assets/file/category-special-equipment-2.jpg",
            "Category": "Экскаваторы погрузчики",
            "TTX1": "Мощность двигателя: 101 л.с.",
            "TTX2": "Экскаваторный ковш: 0.26 м",
            "Brand": "MST (Турция)",
            "Count": 1000,
            "Price": 8211650
        };
        this.product3 = {
            "ID": 3,
            "Name": "Экскаватор погрузчик MST M542",
            "URLImage": "assets/file/category-special-equipment-3.jpg",
            "Category": "Экскаваторы погрузчики",
            "TTX1": "Мощность двигателя: 101 л.с.",
            "TTX2": "Экскаваторный ковш: 0.26 м",
            "Brand": "MST (Турция)",
            "Count": 1000,
            "Price": 8211650
        };
        this.product4 = {
            "ID": 4,
            "Name": "Экскаватор погрузчик MST M542",
            "URLImage": "assets/file/category-special-equipment-4.jpg",
            "Category": "Экскаваторы погрузчики",
            "TTX1": "Мощность двигателя: 101 л.с.",
            "TTX2": "Экскаваторный ковш: 0.26 м",
            "Brand": "MST (Турция)",
            "Count": 1000,
            "Price": 8211650
        }  
    }
}
