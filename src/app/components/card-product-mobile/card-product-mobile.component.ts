import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import formatPrice from '../../utils/formatPrice';
import { CartService } from '../../context/CartContext';

@Component({
  selector: 'app-card-product-mobile',
  templateUrl: './card-product-mobile.component.html',
  styleUrls: ['./card-product-mobile.component.scss']
})

export class CardProductMobileComponent implements OnInit {
  @Input() product: any;
  @Output() onQuantityChange = new EventEmitter<any>();
  @Output() onDelete = new EventEmitter<any>();

  ID: number;
  UrlImage: string;
  Name: string;
  Price: number;
  Cost: number;
  Quantity: number;

  PriceView: string;
  CostView: string;

  cartService: CartService;

  ngOnInit() {
    this.cartService = new CartService();
    console.log(this.product);

    this.ID = this.product.ID;
    this.Name = this.product.Name;
    this.UrlImage = this.product.URLImage;
    this.Price = this.product.Price;
    this.Quantity = this.product.Quantity;

    this.Cost = this.cost;

    this.PriceView = formatPrice(this.product.Price);
    this.CostView = formatPrice(this.cost);
  }

  ngOnChanges() {
    console.log(this.product);
    this.PriceView = formatPrice(this.product.Price);
    this.CostView = formatPrice(this.cost);
    this.Quantity = this.product.Quantity;
  }

  get cost() {
    return this.product.Price * this.product.Quantity;
  }

  quantityChange(product, count) {
    this.onQuantityChange.emit([product, count]);
    product.Quantity += count;
    this.ngOnChanges();
  }

  del (product) {
    this.onDelete.emit(product);
  }
}
