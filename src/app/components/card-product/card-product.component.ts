import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import formatPrice from '../../utils/formatPrice';
import { CartService } from '../../context/CartContext';

@Component({
  selector: '[app-card-product]',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.scss']
})

export class CardProductComponent implements OnInit, OnChanges {
  @Input() product: any;
  @Output() onQuantityChange = new EventEmitter<any>();
  @Output() onDelete = new EventEmitter<any>();

  priceView: number;
  costView: number;
  quantity: number = 1;
  
  cartService: CartService;

  constructor() {
  }

  ngOnInit() {
    this.cartService = new CartService();
    console.log(this.product);
    this.priceView = formatPrice(this.product.Price);
    this.costView = formatPrice(this.cost);
    this.quantity = this.product.Quantity;

  }

  ngOnChanges() {
    console.log(this.product);
    this.priceView = formatPrice(this.product.Price);
    this.costView = formatPrice(this.cost);
    this.quantity = this.product.Quantity;
  }

  onPageChange(): void {
    this.priceView = formatPrice(this.product.Price);
    this.costView = formatPrice(this.cost);
    console.log(this.costView);
  }

  get cost() {
    return this.product.Price * this.product.Quantity;
  }

  quantityChange(product, count) {
    this.onQuantityChange.emit([product, count]);
    product.Quantity += count;
    this.ngOnChanges();
  }

  del(product) {
    this.onDelete.emit(product);
  }
}
