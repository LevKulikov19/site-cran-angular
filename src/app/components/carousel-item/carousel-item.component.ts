import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-carousel-item',
    templateUrl: './carousel-item.component.html',
    styleUrls: ['./carousel-item.component.scss']
})
export class CarouselItemComponent implements OnInit {
    @Input() data: any;
    @Input() active: boolean;
    constructor() { }
  
    title: string;
    link: string;
    img: string

    ngOnInit(): void {
        this.title = this.data.title;
        this.link = this.data.link;
        this.img = this.data.img;
    }
}
