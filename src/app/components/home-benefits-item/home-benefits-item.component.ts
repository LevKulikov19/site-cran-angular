import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-home-benefits-item',
    templateUrl: './home-benefits-item.component.html',
    styleUrls: ['./home-benefits-item.component.scss']
})
export class HomeBenefitsItemComponent implements OnInit {
    @Input() item: any;
    constructor() { }
  
    title: string;
    discription: string;
    img: string

    ngOnInit(): void {
        this.title = this.item.title;
        this.discription = this.item.discription;
        this.img = this.item.img;
    }
}
