import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-home-mst',
    templateUrl: './home-mst.component.html',
    styleUrls: ['./home-mst.component.scss']
})
export class HomeMSTComponent {
    @Input() title: string;
    @Input() description: string;
    @Input() img: string;
}
