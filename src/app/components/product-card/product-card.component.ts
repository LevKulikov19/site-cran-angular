import { Component, Input, OnInit } from '@angular/core';
import formatPrice from '../../utils/formatPrice';


@Component({
    selector: 'app-product-card',
    templateUrl: './product-card.component.html',
    styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
    @Input() product: any;
    modalOpen: boolean = false;
    ID: number;
    Name: string;
    URLImage: string;
    Category: string;
    TTX1: string;
    TTX2: string;
    Brand: string;
    Count: number;
    Price: number;

    Url: string;
    PriceVisible: number;

    constructor() { }

    ngOnInit(): void {
        console.log(this.product);
        this.ID = this.product.ID;
        this.Name = this.product.Name;
        this.URLImage = this.product.URLImage;
        this.Category = this.product.Category;
        this.TTX1 = this.product.TTX1;
        this.TTX2 = this.product.TTX2;
        this.Brand = this.product.Brand;
        this.Count = this.product.Count;
        this.Price = this.product.Price;

        this.Url = '/product/' + this.ID.toString();
        this.PriceVisible = formatPrice(this.Price);
    }

    openModal() {
        this.modalOpen = true;
    }

    closeModal() {
        this.modalOpen = false;
    }

    addToCart() {

    }
}
