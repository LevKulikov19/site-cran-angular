import { Component } from '@angular/core';
import { CartService } from '../../context/CartContext';
import formatPrice from '../../utils/formatPrice';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardComponent {
  products: any[] = [];
  cost: number = 0;
  costView: string;
  cartService: CartService;

  constructor() {
    this.cartService = new CartService();
    this.products = this.cartService.getItems();
    console.log(this.products);
    this.products.forEach(element => {
      this.cost += element.Quantity * element.Price;
    });
    this.costView = formatPrice(this.cost);
  }

  onPageChange(): void {
    console.log(this.products);
    this.products = this.cartService.getItems();
    console.log(this.products);
    this.products.forEach(element => {
      this.cost += element.Quantity * element.Price;
    });
    this.costView = formatPrice(this.cost);
  }

  onDeleteAll() {
    this.cartService.clearCart();
    this.onPageChange();
  }

  onDelete(product: any) {
    this.cartService.removeItem(product.ID);
    this.onPageChange();
  }

  onQuantityChange(e) {
    let product = e[0];
    let count = e[1];
    if (product.Quantity + count <= 0) this.cartService.updateQuantity(product.ID, 1);
    else if (product.Quantity + count >= product.Count) this.cartService.updateQuantity(product.ID, product.Count);
    else this.cartService.updateQuantity(product.ID, product.Quantity + count);
    
    this.products = this.cartService.getItems();
  }

  onBuy() {
    this.cartService.clearCart();
    this.onPageChange();
  }
}