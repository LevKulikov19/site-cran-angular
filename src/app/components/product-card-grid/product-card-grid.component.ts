import { Component, Input, OnInit } from '@angular/core';
import formatPrice from '../../utils/formatPrice';


@Component({
    selector: 'app-product-card-grid',
    templateUrl: './product-card-grid.component.html',
    styleUrls: ['./product-card-grid.component.scss']
})
export class ProductCardGridComponent implements OnInit {
    @Input() products: any[];
    @Input() rows: number;
    @Input() columns: number;
    chunkedProducts: any[];

    constructor() { }

    ngOnInit(): void {
        this.chunkedProducts = this.grid(this.products, this.rows, this.columns);
    }

    ngOnChanges(): void {
        this.chunkedProducts = this.grid(this.products, this.rows, this.columns);
    }

    grid(products, rows, columns) {
        let matrix = [];
        for (let i = 0; i < rows; i++) {
            let row = [];
            for (let j = 0; j < columns; j++) {
                if (products[i * columns + j] == undefined) {
                    break;
                }
                row.push(products[i * columns + j]);
            }
            matrix.push(row);
        }
        return matrix;
    }
}

