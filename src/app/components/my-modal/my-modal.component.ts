import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-my-modal',
  templateUrl: './my-modal.component.html',
  styleUrls: ['./my-modal.component.scss']
})
export class MyModalComponent {
  @Input() visible: boolean;
  @Output() visibleChange = new EventEmitter<boolean>();

  ngOnChanges(): void {
    console.log(this.visible);
  }

  closeModal() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }
}
