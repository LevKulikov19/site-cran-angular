import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
    imgCard: string;
  
    constructor() { }
  
    ngOnInit(): void {
      this.imgCard = "assets/file/icon-card.svg";
    }
  }