import { Component, OnInit, Input } from '@angular/core';
import formatPrice from '../../utils/formatPrice';
import { CartService } from '../../context/CartContext';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-details-component',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  @Input() product: any;
  ID: number;
  Name: string;
  URLImage: string;
  Category: string;
  TTX1: string;
  TTX2: string;
  Brand: string;
  Count: number;
  Price: number;

  PriceVisible: number;
  quantity: number = 1;
  
  cartService: CartService;


  constructor( private router: Router ) {

  }

  ngOnInit() {
    this.cartService = new CartService();

    this.ID = this.product.ID;
    this.Name = this.product.Name;
    this.URLImage = this.product.URLImage;
    this.Category = this.product.Category;
    this.TTX1 = this.product.TTX1;
    this.TTX2 = this.product.TTX2;
    this.Brand = this.product.Brand;
    this.Count = this.product.Count;
    this.Price = this.product.Price;

    this.PriceVisible = formatPrice(this.Price);
  }

  addToCart() {
    console.log(this.quantity);
    this.cartService.addItem(this.product, this.quantity);
    this.router.navigate(['/cart']);
  }

  onInputQuantity(event) {
    this.quantity = event.target.value;
  }
}