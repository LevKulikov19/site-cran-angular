import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
  @Input() count: number;
  @Input() active: number;
  @Input() path: string;
  @Output() pageChange = new EventEmitter<number>();
  pages: number[];
  
  constructor(private router: Router) {

  }

  ngOnChanges(): void {
    console.log(this.active);
    this.pages = Array.from({length: this.count}, (_, i) => i + 1);
  }

  onPageChange(page: number): void {
    this.pageChange.emit(page);
  }
}