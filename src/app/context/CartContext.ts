import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

// Определение действий
const ACTIONS = {
    ADD_ITEM: 'add-item',
    REMOVE_ITEM: 'remove-item',
    UPDATE_QUANTITY: 'update-quantity',
    CLEAR_CART: 'clear-cart'
};


export class CartService {
    private cartSubject = new BehaviorSubject([]);
    cart$ = this.cartSubject.asObservable();

    constructor() {
        const localData = localStorage.getItem('cart');
        if (localData) {
            this.cartSubject.next(JSON.parse(localData));
        }
    }

    private cartReducer(state, action) {
        switch (action.type) {
            case ACTIONS.ADD_ITEM:
                const existingItemIndex = state.findIndex(item => item.ID === action.payload.ID);
                if (existingItemIndex >= 0) {
                    const newState = [...state];
                    const q = parseInt(action.payload.Quantity) + parseInt(newState[existingItemIndex].Quantity);
                    newState[existingItemIndex].Quantity = q;
                    return newState;
                } else {
                    return [...state, action.payload];
                }
            case ACTIONS.REMOVE_ITEM:
                return state.filter(item => item.ID !== action.payload.ID);
            case ACTIONS.UPDATE_QUANTITY:
                return state.map(item => item.ID === action.payload.ID ? { ...item, Quantity: action.payload.Quantity } : item);
            case ACTIONS.CLEAR_CART:
                return [];
            default:
                return state;
        }
    }

    private dispatch(action) {
        const newState = this.cartReducer(this.cartSubject.value, action);
        this.cartSubject.next(newState);
        localStorage.setItem('cart', JSON.stringify(newState));
    }

    addItem(item, Quantity) {
        this.dispatch({ type: ACTIONS.ADD_ITEM, payload: { ...item, Quantity } });
    }

    removeItem(ID) {
        this.dispatch({ type: ACTIONS.REMOVE_ITEM, payload: { ID } });
    }

    updateQuantity(ID, Quantity) {
        this.dispatch({ type: ACTIONS.UPDATE_QUANTITY, payload: { ID, Quantity } });
    }

    clearCart() {
        this.dispatch({ type: ACTIONS.CLEAR_CART });
    }

    getItems() {
        return this.cartSubject.value;
    }
}