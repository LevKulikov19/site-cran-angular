import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'

import {AppRoutingModule} from './app-routing.module'
import {AppComponent} from './app.component'

import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component'
import { HeaderCatalogItemComponent } from './components/header-catalog-item/header-catalog-item.component';
import { HomeAboutUsComponent } from './components/home-about-us/home-about-us.component';
import { HomePageComponent } from './pages/home/home.component';
import { HomeMSTComponent } from './components/home-mst/home-mst.component';
import { HomeBenefitsComponent } from './components/home-benefits/home-benefits.component';
import { HomeBenefitsItemComponent } from './components/home-benefits-item/home-benefits-item.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { CarouselItemComponent } from './components/carousel-item/carousel-item.component';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductPageComponent } from './pages/product/product.component';
import { HomeProductCardComponent } from './components/home-product-card/home-product-card.component';
import { CategoryPageComponent } from './pages/category/category.component';
import { CatalogPageComponent } from './pages/catalog/catalog.component';
import { ProductCardGridComponent } from './components/product-card-grid/product-card-grid.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { MyModalComponent } from './components/my-modal/my-modal.component';
import { ProductCardMoadalComponent } from './components/product-card-modal/product-card-modal.component';
import { CardComponent } from './components/card-details/card-details.component';
import { CardProductComponent } from './components/card-product/card-product.component';
import { CardProductMobileComponent } from './components/card-product-mobile/card-product-mobile.component';
import { CartPageComponent } from './pages/cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    MyModalComponent,
    FooterComponent,
    HeaderComponent,
    HomeAboutUsComponent,
    HeaderCatalogItemComponent,
    HomePageComponent,
    HomeMSTComponent,
    HomeBenefitsComponent,
    HomeBenefitsItemComponent,
    CarouselComponent,
    CarouselItemComponent,
    ProductCardComponent,
    ProductCardMoadalComponent,
    HomeProductCardComponent,
    
    ProductPageComponent,
    ProductDetailsComponent,

    CategoryPageComponent,

    ProductCardGridComponent,
    PaginationComponent,
    CatalogPageComponent,

    CartPageComponent,
    CardComponent,
    CardProductComponent,
    CardProductMobileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}