import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomePageComponent implements OnInit {
  aboutUsTitle: string
  aboutUsDiscription: string 
  MSTTitle: string
  MSTDiscription: string 
  MSTImg: string
  benefitsItems: Array<Object>
  carouselItems: Array<Object>

  constructor() { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.carouselItems = [
      {
          img: "assets/file/1.png",
          link: "/catalog/special-equipment/1",
          title: "Спецтехника"
      },
      {
          img: "assets/file/2.png",
          link: "/catalog/attachments/1",
          title: "Навесное оборудование"
      },
      {
          img: "assets/file/3.png",
          link: "/catalog/road-equipment/1",
          title: "Дорожное оснащение"
      },
      {
          img: "assets/file/4.png",
          link: "/catalog/materials/1",
          title: "Материалы"
      }
  ]

    this.aboutUsTitle =  "Компания «Кран» - ваш надежный поставщик спецтехники и дорожного оборудования";
    this.aboutUsDiscription  = 
    'На нашем гипермаркете "Велес" вы сможете найти и приобрести специальную технику от лидирующих мировых' +
    'производителей: Caterpillar, Kaiser, Komatsu, Liebherr, Menzi Muck и многих других. Доставка осуществляется из' +
    'Китая, Кореи, Германии, Италии и США в любую точку России.' +
    'Кроме собственных предложений, мы также предлагаем всем собственникам и производителям разместить объявления о' +
    'продаже на нашем маркетплейсе.' +
    'Наш специализированный маркетплейс ТСК "Велес" уже более 10 лет успешно работает на рынке продажи различных' +
    'типов техники и оборудования, предоставляя своим клиентам возможность совершать выгодные и безопасные покупки со' +
    'всего мира.';
    this.MSTTitle = "Экскаваторы погрузчики MST";
    this.MSTDiscription = 
    "Компания Кран рада предложить вам возможность приобрести экскаваторы-погрузчики от официального дилера MST в" +
    "России. В нашем ассортименте представлено 8 различных моделей с разными модификациями. Мы предлагаем как" +
    "абсолютно новые экскаваторы-погрузчики с официальной гарантией производителя, так и те машины, которые ранее" +
    "находились в эксплуатации, но прошли тщательную предпродажную подготовку в нашей сервисной службе." +
    "Мы осуществляем продажу экскаваторов-погрузчиков MST на всей территории Российской Федерации.";
    this.MSTImg = "assets/file/5.png";

    this.benefitsItems = [
      {
          img: "assets/file/icon-a-1.png",
          discription: "Более чем 20-летний опыт",
          title: "Опыт работы"
      },
      {
          img: "assets/file/icon-a-2.png",
          discription: "Максимальная оперативность исполнения заказа. Отсутствие ограничений по объёму заказа.",
          title: "Оперативность"
      },
      {
          img: "assets/file/icon-a-3.png",
          discription: "Индивидуальные или стандартные технические решения, в зависимости от пожеланий заказчика.",
          title: "Индивидуальный подход"
      },
      {
          img: "assets/file/icon-a-4.png",
          discription: "Профессиональная техническая поддержка 24x7",
          title: "Техподдержка"
      }
  ]
  }

}