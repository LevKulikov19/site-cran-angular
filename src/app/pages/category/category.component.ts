import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category-page',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryPageComponent implements OnInit {
  constructor() { 
  }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}