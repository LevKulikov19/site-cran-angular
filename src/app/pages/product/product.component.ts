import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../../context/CartContext';
import { getById, getCategoryByProductId } from '../../product/ProductService'

@Component({
  selector: 'app-product-page',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductPageComponent implements OnInit {
  product: any;
  quantity = 1;
  card: CartService;

  category: string;
  name: string;
  urlCategory: string;

  id: number = 0;
  constructor(private router: Router) { 
    window.scrollTo(0, 0);
    this.card = new CartService();
    
    let id = this.router.url.split('/').slice(-1)[0];
    console.log(id)
    this.product = getById(id);
    console.log(this.product)

    let c = getCategoryByProductId(id)
    this.category = c.Category;
    this.name = this.product.Name;
    console.log(c);
    this.urlCategory = c.CategoryLink;
  }

  ngOnInit(): void {
  }

  addItem(product, quantity) {
    this.card.addItem(product, quantity);
  }
}
