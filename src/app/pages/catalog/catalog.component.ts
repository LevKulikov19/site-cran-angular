import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { getAllByCategory, getCategoryById, getCountByCategory } from '../../product/ProductService'

@Component({
  selector: 'app-catalog-page',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogPageComponent implements OnInit {
  row = 2;
  columns = 4;
  countPage: number;
  category: any;
  products: any[];
  page: number;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.route.params.subscribe(params => {
      console.log('changea', params['page']);
      this.page = params['page'];
      this.category = getCategoryById(params['category']);
      this.products = getAllByCategory(this.category.CategoryID, this.row * this.columns, this.page);
      this.countPage = Math.ceil(getCountByCategory(params['category']) / this.row / this.columns);
    });
  }

  onPageChange(page: number): void {
    console.log('change', page);
    
  }
}