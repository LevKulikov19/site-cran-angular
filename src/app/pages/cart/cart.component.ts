import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartPageComponent implements OnInit {
  constructor() { 
  }

  ngOnInit(): void {
    window.scrollTo(0, 0);
  }

}