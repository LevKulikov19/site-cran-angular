export default function formatPrice(price) {
    console.log(price);
    if (price == undefined) {
        return '';
    }
    const formattedPrice = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    return `${formattedPrice} руб`;
}
