var productData = [
    {
        "Category": "Cпецтехника",
        "CategoryID": "special-equipment",
        "CategoryLink": "/catalog/special-equipment/1",
        "Products": [
            {
                "ID": 1,
                "Name": "Экскаватор погрузчик MST M542",
                "URLImage": "assets/file/category-special-equipment-1.jpg",
                "Category": "Экскаваторы погрузчики",
                "TTX1": "Мощность двигателя: 101 л.с.",
                "TTX2": "Экскаваторный ковш: 0.26 м",
                "Brand": "MST (Турция)",
                "Count": 1000,
                "Price": 8211650
            },
            {
                "ID": 2,
                "Name": "Экскаватор погрузчик MST M642 Plus",
                "URLImage": "assets/file/category-special-equipment-2.jpg",
                "Category": "Экскаваторы погрузчики",
                "TTX1": "Мощность двигателя: 101 л.с.",
                "TTX2": "Экскаваторный ковш: 0.26 м",
                "Brand": "MST (Турция)",
                "Count": 1000,
                "Price": 9439849
            },
            {
                "ID": 3,
                "Name": "Трактор-погрузчик ПБТК-764 СТАНИСЛАВ",
                "URLImage": "assets/file/category-special-equipment-3.jpg",
                "Category": "Трактора",
                "TTX1": "Мощность двигателя: 240-330 л.с.",
                "TTX2": "Масса: 18 т Базовый трактор",
                "Brand": "MST (Турция)",
                "Count": 1000,
                "Price": 13750000
            },
            {
                "ID": 4,
                "Name": "Самосвал Shacman F3000 8х4 SX3316DV366",
                "URLImage": "assets/file/category-special-equipment-4.jpg",
                "Category": "Самосвалы",
                "TTX1": "Грузоподъемность: - кг",
                "TTX2": "Объем кузова: - м",
                "Brand": "Shacman",
                "Count": 1000,
                "Price": 4750000
            },
            {
                "ID": 5,
                "Name": "Экскаватор погрузчик LEIST GROSS 788",
                "URLImage": "assets/file/category-special-equipment-5.jpg",
                "Category": "Экскаваторы погрузчики",
                "TTX1": "Мощность двигателя: 100 л.с.",
                "TTX2": "Экскаваторный ковш: 0.3 м3",
                "Brand": "Leist Gross (Китай)",
                "Count": 1000,
                "Price": 7000000
            },
            {
                "ID": 6,
                "Name": "Бульдозер Б10М - Двигатель Д180 К/Р 1 2021",
                "URLImage": "assets/file/category-special-equipment-6.jpg",
                "Category": "Бульдозеры",
                "TTX1": "Масса: 19,5 т",
                "TTX2": "Мощность двигателя: 180 л.с.",
                "Brand": "Велес (Россия)",
                "Count": 1000,
                "Price": 7000000
            },
            {
                "ID": 7,
                "Name": "Седельный тягач FAW JH6 ORYX4",
                "URLImage": "assets/file/category-special-equipment-7.jpg",
                "Category": "Тягачи",
                "TTX1": " ",
                "TTX2": " ",
                "Brand": "FAW (Китай)",
                "Count": 1000,
                "Price": 9600000
            },
            {
                "ID": 8,
                "Name": "Самосвал МАЗ 6501С9-8520-005",
                "URLImage": "assets/file/category-special-equipment-8.jpg",
                "Category": "Самосвалы",
                "TTX1": "Грузоподъемность: 19500 кг",
                "TTX2": "Объем кузова: - м3",
                "Brand": "МАЗ",
                "Count": 1000,
                "Price": 9439849
            },
            {
                "ID": 9,
                "Name": "Экскаватор гусеничный JCB JS 330",
                "URLImage": "assets/file/category-special-equipment-9.jpg",
                "Category": "Гусеничные экскаваторы",
                "TTX1": "Мощность: 285 л.с.",
                "TTX2": "Масса: 33,7 тонн",
                "Brand": "JCB (Великобритания)",
                "Count": 1000,
                "Price": 9439849
            },
            {
                "ID": 10,
                "Name": "Экскаватор гусеничный JCB JS 130",
                "URLImage": "assets/file/category-special-equipment-10.jpg",
                "Category": "Гусеничные экскаваторы",
                "TTX1": "Мощность: 100,6 л.с.",
                "TTX2": "Масса: 13,6 т",
                "Brand": "JCB (Великобритания)",
                "Count": 1000,
                "Price": 9474980
            },
            {
                "ID": 11,
                "Name": "Экскаватор гусеничный JCB-JS 190",
                "URLImage": "assets/file/category-special-equipment-11.jpg",
                "Category": "Гусеничные экскаваторы",
                "TTX1": "Мощность: 131,8 л.с.",
                "TTX2": "Масса: 19,9 т",
                "Brand": "JCB (Великобритания)",
                "Count": 1000,
                "Price": 13750000
            }
        ]
    },
    {
        "Category": "Навесное оборудование",
        "CategoryID": "special-attachments",
        "CategoryLink": "/catalog/special-attachments/1",
        "Products": [
            {
                "ID": 21,
                "Name": "Дробильный ковш MB BF90.3 S4",
                "URLImage": "assets/file/attachment-1.jpg",
                "Category": "Дробильные ковши",
                "TTX1": "Рекомендуемая машина —",
                "TTX2": "экскаватор",
                "Brand": "MST (Турция)",
                "Count": 1000,
                "Price": 211650
            },
            {
                "ID": 22,
                "Name": "Погрузчик на МТЗ Турс-2000-0 без джойстика",
                "URLImage": "assets/file/attachment-2.jpg",
                "Category": "Погрузчики",
                "TTX1": "Рекомендуемая машина —",
                "TTX2": "экскаватор",
                "Brand": "MST (Турция)",
                "Count": 1000,
                "Price": 325900
            },
            {
                "ID": 23,
                "Name": "Рециклер асфальтобетона НВ-1",
                "URLImage": "assets/file/attachment-3.jpg",
                "Category": "Рециклеры",
                "TTX1": "Объем: 0,4 куб.м.",
                "TTX2": " ",
                "Brand": "Ticab (Польша)",
                "Count": 1000,
                "Price": 325900
            },
            {
                "ID": 24,
                "Name": "Рециклер асфальтобетона RA-800",
                "URLImage": "assets/file/attachment-4.jpg",
                "Category": "Рециклеры",
                "TTX1": "Производительность:",
                "TTX2": "1800-2100 кг",
                "Brand": "Ticab (Польша)",
                "Count": 1000,
                "Price": 2234097
            }
        ]
    },
    {
        "Category": "Дорожное оснащение",
        "CategoryID": "road-equipment",
        "CategoryLink": "/catalog/road-equipment/1",
        "Products": [
            {
                "ID": 31,
                "Name": "Знак 2. 5 Движение без остановки запрещено",
                "URLImage": "assets/file/road-equipment-1.jpg",
                "Category": "Дорожные знаки",
                "TTX1": "Типоразмеры: 1, 2, 3",
                "TTX2": "Тип пленки: А, Б, В",
                "Brand": "Велес (Россия)",
                "Count": 1000,
                "Price": 1050
            }, {
                "ID": 32,
                "Name": "Знак 3.2 Движение запрещено",
                "URLImage": "assets/file/road-equipment-2.jpg",
                "Category": "Дорожные знаки",
                "TTX1": "Типоразмеры: 1, 2, 3",
                "TTX2": "Тип пленки: А, Б, В",
                "Brand": "Велес (Россия)",
                "Count": 1000,
                "Price": 1050
            }, {
                "ID": 33,
                "Name": "Дорожный знак светодиодный 900 4.2.3 Эконом",
                "URLImage": "assets/file/road-equipment-3.jpg",
                "Category": "Дорожные знаки",
                "TTX1": "Типоразмеры: 1, 2, 3",
                "TTX2": "Тип пленки: А, Б, В",
                "Brand": "Велес (Россия)",
                "Count": 1000,
                "Price": 9500
            }, {
                "ID": 34,
                "Name": "Знак 2. 2 Конец главной дороги",
                "URLImage": "assets/file/road-equipment-4.jpg",
                "Category": "Дорожные знаки",
                "TTX1": "Типоразмеры: 1, 2, 3",
                "TTX2": "Тип пленки: А, Б, В",
                "Brand": "Велес (Россия)",
                "Count": 1000,
                "Price": 1000
            }
        ]
    },
    {
        "Category": "Материалы",
        "CategoryID": "materials",
        "CategoryLink": "/catalog/materials/1",
        "Products": [
            {
                "ID": 41,
                "Name": "Дорожная краска АК-511 «Спринтер» цветная",
                "URLImage": "assets/file/material-1.jpg",
                "Category": "Материалы для разметки",
                "TTX1": "Долгосохнущая краска",
                "TTX2": " ",
                "Brand": "Велес (Россия)",
                "Count": 1000,
                "Price": 6190
            },
            {
                "ID": 42,
                "Name": "Дорожная краска АК-511 «Спринтер» белая",
                "URLImage": "assets/file/material-2.jpg",
                "Category": "Материалы для разметки",
                "TTX1": "Долгосохнущая краска",
                "TTX2": " ",
                "Brand": "Велес (Россия)",
                "Count": 1000,
                "Price": 5600
            },
            {
                "ID": 43,
                "Name": "Дорожная краска АК-505 \"Road Expert\" белая",
                "URLImage": "assets/file/material-3.jpg",
                "Category": "Дорожная краска",
                "TTX1": "Долгосохнущая краска",
                "TTX2": " ",
                "Brand": "Велес (Россия)",
                "Count": 1000,
                "Price": 11600
            }
        ]
    }
];

export function getAllByCategory(categoryID, limit = 10, page = 1) {
    let products = [];
    let offset = limit * (page - 1);
    let counter = 0;
    productData.forEach((catalogCategory) => {
        if (catalogCategory.CategoryID === categoryID) {
            catalogCategory.Products.forEach((productItem) => {
                if (counter >= offset) {
                    if (products.length >= limit) {
                        return false;
                    }
                    products.push(productItem);

                }
                counter++;

            });
        }
    });
    return products;
}

export function getCountByCategory(categoryID) {
    let counter = 0;
    productData.forEach((catalogCategory) => {
        if (catalogCategory.CategoryID === categoryID) {
            catalogCategory.Products.forEach((productItem) => {
                counter++;
            });
        }
    });
    return counter;
}

export function getById(id) {
    let product = undefined;
    productData.forEach((catalogCategory) => {
        catalogCategory.Products.forEach((productItem) => {
            if (productItem.ID == parseInt(id)) {
                product = productItem;
                return false;
            };
        });
    });
    return product;
}

export function getCategoryById(id) {
    let category = undefined;
    productData.forEach((catalogCategory) => {
        if (catalogCategory.CategoryID == id) {
            category = catalogCategory;
            return false;
        }
    });
    return category;
}

export function getCategoryByProductId(id) {
    let category = undefined;
    productData.forEach((catalogCategory) => {
        catalogCategory.Products.forEach((productItem) => {
            if (productItem.ID == parseInt(id)) {
                category = catalogCategory;
                return false;
            };
        });
    });
    return category;
}